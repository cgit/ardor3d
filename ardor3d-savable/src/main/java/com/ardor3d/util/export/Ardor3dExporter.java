/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.util.export;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public interface Ardor3dExporter {

    /**
     * Save a Savable object to the given stream.
     *
     * @param savable
     *            the savable object
     * @param os
     *            the output stream
     * @throws IOException
     *             exception
     */
    void save(Savable savable, OutputStream os) throws IOException;

    /**
     * Save a Savable object to the given file.
     *
     * @param savable
     *            the savable object
     * @param file
     *            the file
     * @throws IOException
     *             exception
     */
    void save(Savable savable, File file) throws IOException;
}
