/**
 * Copyright (c) 2008-2021 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.math;

/**
 * Just a simple flag holder for runtime stripping of various ardor3d logging and debugging features.
 */
public class MathConstants {

    public static final boolean useMathPools = System.getProperty("ardor3d.noMathPools") == null;

    public static final boolean useFastMath = System.getProperty("ardor3d.useFastMath") != null;

    public static final int maxMathPoolSize = Integer.parseInt(System.getProperty("ardor3d.maxMathPoolSize", "11"));

    /**
     * Flag indicating whether zero floating-point representations's sign is taken into account when comparing values.
     * <code>true</code> when -0.0 and 0.0 are considered equal, otherwise <code>false</code>. Ignoring the sign
     * produces a numerically correct behavior but is inconsistent with java.lang.Double.compare()
     */
    public static final boolean ignoreZeroFloatingPointRepresentationSign = System.getProperty("ardor3d.useZeroFloatingPointRepresentationSign") == null;
}
