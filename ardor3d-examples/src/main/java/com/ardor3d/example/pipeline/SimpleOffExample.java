/**
 * Copyright (c) 2008-2023 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.example.pipeline;

import com.ardor3d.example.ExampleBase;
import com.ardor3d.example.Purpose;
import com.ardor3d.extension.model.off.OffGeometryStore;
import com.ardor3d.extension.model.off.OffImporter;
import com.ardor3d.math.Vector3;
import com.ardor3d.scenegraph.Node;

/**
 * Simplest example of loading a OFF model.
 */
@Purpose(htmlDescriptionKey = "com.ardor3d.example.pipeline.SimpleOffExample", //
        thumbnailPath = "com/ardor3d/example/media/thumbnails/pipeline_SimpleOffExample.jpg", //
        maxHeapMemory = 64)
public class SimpleOffExample extends ExampleBase {
    public static void main(final String[] args) {
        ExampleBase.start(SimpleOffExample.class);
    }

    @Override
    protected void initExample() {
        _canvas.setTitle("Ardor3D - Simple Off Example");
        _canvas.getCanvasRenderer().getCamera().setLocation(new Vector3(3.5, 1.5, 5));

        // Load the OFF scene
        final long time = System.currentTimeMillis();
        final OffImporter importer = new OffImporter();
        final OffGeometryStore storage = importer.load("off/vertcube.off");
        System.out.println("Importing Took " + (System.currentTimeMillis() - time) + " ms");

        final Node model = storage.getScene();
        // the off model is usually z-up - switch to y-up, isn't it?
        // model.setRotation(new Quaternion().fromAngleAxis(-MathUtils.HALF_PI, Vector3.UNIT_X));
        _root.attachChild(model);
    }
}
