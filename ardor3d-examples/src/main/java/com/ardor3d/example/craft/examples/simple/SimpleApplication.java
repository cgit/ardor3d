
package com.ardor3d.example.craft.examples.simple;

import com.ardor3d.example.craft.base.ArdorBaseApplication;

public class SimpleApplication extends ArdorBaseApplication {

    public SimpleApplication() {
        super(new SimpleGame());
    }

    public static void main(final String[] args) {
        final ArdorBaseApplication example = new SimpleApplication();
        new Thread(example, "MainArdorThread").start();
    }
}
