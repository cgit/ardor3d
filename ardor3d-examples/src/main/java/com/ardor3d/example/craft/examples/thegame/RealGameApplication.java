
package com.ardor3d.example.craft.examples.thegame;

import com.ardor3d.example.craft.base.ArdorBaseApplication;

public class RealGameApplication extends ArdorBaseApplication {

    public RealGameApplication() {
        super(new RealGame());
    }

    public static void main(final String[] args) {
        final ArdorBaseApplication example = new RealGameApplication();
        new Thread(example, "MainArdorThread").start();
    }
}
