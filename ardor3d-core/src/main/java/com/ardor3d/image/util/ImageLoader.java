/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.image.util;

import java.io.IOException;
import java.io.InputStream;

import com.ardor3d.image.Image;

/**
 * Interface for image loaders. Implementing classes can be registered with the TextureManager to decode image formats
 * with a certain file extension.
 *
 * @see com.ardor3d.image.util.ImageLoaderUtil#registerHandler(ImageLoader, String...)
 *
 */
public interface ImageLoader {

    /**
     * Decodes image data from an InputStream.
     *
     * @param is
     *            The InputStream to create the image from. The input stream should be closed before this method
     *            returns.
     * @param flipped
     *            <code>true</code> if the image is vertically flipped
     * @return The decoded Image.
     * @throws IOException
     *             if something wrong occurs while loading the image from the input stream
     */
    public Image load(InputStream is, boolean flipped) throws IOException;
}