/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.renderer;

import com.ardor3d.framework.DisplaySettings;

public interface TextureRendererProvider {

    /**
     * @see TextureRendererFactory#createTextureRenderer(int, int, Renderer, ContextCapabilities)
     * @param width
     *            the width of our off screen rendering target
     * @param height
     *            the height of our off screen rendering target
     * @param renderer
     *            the renderer to use when rendering to this off screen target.
     * @param caps
     *            the context capabilities, used for testing.
     * @return a texture renderer
     */
    TextureRenderer createTextureRenderer(int width, int height, Renderer renderer, ContextCapabilities caps);

    /**
     * @see TextureRendererFactory#createTextureRenderer(int, int, int, int, Renderer, ContextCapabilities)
     * @param width
     *            the width of our off screen rendering target
     * @param height
     *            the height of our off screen rendering target
     * @param depthBits
     *            the desired depth buffer size of our off screen rendering target
     * @param samples
     *            the number of samples for our off screen rendering target
     * @param renderer
     *            the renderer to use when rendering to this off screen target.
     * @param caps
     *            the context capabilities, used for testing.
     * @return a texture renderer
     */
    TextureRenderer createTextureRenderer(int width, int height, int depthBits, int samples, Renderer renderer,
            ContextCapabilities caps);

    /**
     * @see TextureRendererFactory#createTextureRenderer(DisplaySettings, boolean, Renderer, ContextCapabilities)
     * @param settings
     *            a complete set of possible display settings to use. Some will only be valid if Pbuffer is used.
     * @param forcePbuffer
     *            if <code>true</code>, we will return a pbuffer or null if pbuffers are not supported.
     * @param renderer
     *            the renderer to use when rendering to this off screen target.
     * @param caps
     *            the context capabilities, used for testing.
     * @return a texture renderer
     */
    TextureRenderer createTextureRenderer(DisplaySettings settings, boolean forcePbuffer, Renderer renderer,
            ContextCapabilities caps);

}
