/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.util.scenegraph;

import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import com.ardor3d.renderer.ContextCleanListener;
import com.ardor3d.renderer.ContextManager;
import com.ardor3d.renderer.RenderContext;
import com.ardor3d.renderer.Renderer;
import com.ardor3d.renderer.RendererCallable;
import com.ardor3d.scenegraph.Spatial;
import com.ardor3d.util.GameTaskQueueManager;
import com.ardor3d.util.SimpleContextIdReference;

public class DisplayListDelegate implements RenderDelegate {

    private static Map<DisplayListDelegate, Object> _identityCache = new WeakHashMap<>();
    private static final Object STATIC_REF = new Object();

    private static ReferenceQueue<DisplayListDelegate> _refQueue = new ReferenceQueue<>();

    static {
        ContextManager.addContextCleanListener(new ContextCleanListener() {
            @Override
            public void cleanForContext(final RenderContext renderContext) {
                // TODO: Need a way to call back to the creator of the display list?
            }
        });
    }

    private final SimpleContextIdReference<DisplayListDelegate> _id;

    public DisplayListDelegate(final int id, final Object glContext) {
        _id = new SimpleContextIdReference<>(this, _refQueue, id, glContext);
        _identityCache.put(this, STATIC_REF);
    }

    @Override
    public void render(final Spatial spatial, final Renderer renderer) {
        // do transforms
        final boolean transformed = renderer.doTransforms(spatial.getWorldTransform());

        // render display list.
        renderer.renderDisplayList(_id.getId());

        // Our states are in an unknown state at this point, so invalidate tracking.
        ContextManager.getCurrentContext().invalidateStates();

        // undo transforms
        if (transformed) {
            renderer.undoTransforms(spatial.getWorldTransform());
        }
    }

    public static void cleanAllDisplayLists(final Renderer deleter) {
        final Map<Object, List<Integer>> idMap = new HashMap<>();

        // gather up expired Display Lists... these don't exist in our cache
        gatherGCdIds(idMap);

        // Walk through the cached items and delete those too.
        for (final DisplayListDelegate buf : _identityCache.keySet()) {
            // Add id to map
            idMap.compute(buf._id.getGlContext(), (final Object currentKey, final List<Integer> oldValue) -> {
                final List<Integer> newValue;
                if (oldValue == null) {
                    newValue = new ArrayList<>();
                } else {
                    newValue = oldValue;
                }
                newValue.add(Integer.valueOf(buf._id.getId()));
                return newValue;
            });
        }

        handleDisplayListDelete(deleter, idMap);
    }

    public static void cleanExpiredDisplayLists(final Renderer deleter) {
        // gather up expired display lists...
        final Map<Object, List<Integer>> idMap = gatherGCdIds(null);

        if (idMap != null) {
            // send to be deleted on next render.
            handleDisplayListDelete(deleter, idMap);
        }
    }

    @SuppressWarnings("unchecked")
    private static Map<Object, List<Integer>> gatherGCdIds(Map<Object, List<Integer>> store) {
        // Pull all expired display lists from ref queue and add to an id multimap.
        SimpleContextIdReference<DisplayListDelegate> ref;
        while ((ref = (SimpleContextIdReference<DisplayListDelegate>) _refQueue.poll()) != null) {
            if (store == null) { // lazy init
                store = new HashMap<>();
            }
            final SimpleContextIdReference<DisplayListDelegate> currentRef = ref;
            store.compute(ref.getGlContext(), (final Object currentKey, final List<Integer> oldValue) -> {
                final List<Integer> newValue;
                if (oldValue == null) {
                    newValue = new ArrayList<>();
                } else {
                    newValue = oldValue;
                }
                newValue.add(Integer.valueOf(currentRef.getId()));
                return newValue;
            });
            ref.clear();
        }
        return store;
    }

    private static void handleDisplayListDelete(final Renderer deleter, final Map<Object, List<Integer>> idMap) {
        Object currentGLRef = null;
        // Grab the current context, if any.
        if (deleter != null && ContextManager.getCurrentContext() != null) {
            currentGLRef = ContextManager.getCurrentContext().getGlContextRep();
        }
        // For each affected context...
        for (final Map.Entry<Object, List<Integer>> idMapEntry : idMap.entrySet()) {
            final Object glref = idMapEntry.getKey();
            // If we have a deleter and the context is current, immediately delete
            if (deleter != null && glref.equals(currentGLRef) && idMapEntry.getValue() != null) {
                deleter.deleteDisplayLists(idMapEntry.getValue());
            }
            // Otherwise, add a delete request to that context's render task queue.
            else {
                GameTaskQueueManager.getManager(ContextManager.getContextForRef(glref))
                        .render(new RendererCallable<Void>() {
                            @Override
                            public Void call() throws Exception {
                                if (idMapEntry.getValue() != null) {
                                    getRenderer().deleteDisplayLists(idMapEntry.getValue());
                                }
                                return null;
                            }
                        });
            }
        }
    }
}
