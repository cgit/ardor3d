/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.util;

import java.util.Comparator;

/**
 * Shell and merge sort implementations with the goal of reducing garbage and allowing tuning.
 */
public abstract class SortUtil {

    /**
     * The size at or below which we will use shell sort instead of the system sort.
     */
    public static int SHELL_SORT_THRESHOLD = 17;

    /**
     * Merge sorts the supplied data, in the given range, using the given comparator.
     *
     * <b>Note: this internally creates a temporary copy of the array to use as work space during sort.</b>
     *
     * @param <T>
     *            the type of data to sort
     *
     * @param source
     *            the array to sort. Will hold the sorted array on completion.
     * @param left
     *            the left-most index of our sort range.
     * @param right
     *            the right-most index of our sort range.
     * @param comp
     *            our object Comparator
     */
    @SuppressWarnings("unchecked")
    public static <T> void msort(final T[] source, final int left, final int right, final Comparator<? super T> comp) {
        final T[] copy = (T[]) new Object[source.length];
        System.arraycopy(source, 0, copy, 0, source.length);
        msort(copy, source, left, right, comp);
    }

    /**
     * Merge sorts the supplied data, in the given range, using the given comparator.
     *
     * @param <T>
     *            the type of data to sort
     * @param source
     *            contains the elements to be sorted and acts as a work space for the sort.
     * @param copy
     *            contains the elements to be sorted and will hold the fully sorted array when complete.
     * @param left
     *            the left-most index of our sort range.
     * @param right
     *            the right-most index of our sort range.
     * @param comp
     *            our object Comparator
     */
    public static <T> void msort(final T[] source, final T[] copy, final int left, final int right,
            final Comparator<? super T> comp) {
        // use an insertion sort on small arrays to avoid recursion down to 1:1
        final int length = right - left + 1;
        if (length <= SHELL_SORT_THRESHOLD) {
            // insertion sort in place
            shellSort(copy, left, right, comp);
            // copy into destination
            return;
        }

        // recursively sort each half of array
        final int mid = (left + right) >> 1;
        msort(copy, source, left, mid, comp);
        msort(copy, source, mid + 1, right, comp);

        // merge the sorted halves
        merge(source, copy, left, mid, right, comp);
    }

    /**
     * Performs a merge on two sets of data stored in source, represented by the ranges formed by [left, mid] and
     * [mid+1, right]. Stores the result in destination.
     *
     * @param <T>
     *            the type of data to sort
     * @param source
     *            our source data
     * @param destination
     *            the array to store our result in
     * @param left
     *            the left index of the range to sort
     * @param mid
     *            the middle index of the range to sort
     * @param right
     *            the right index of the range to sort
     * @param comp
     *            our object Comparator
     */
    protected static <T> void merge(final T[] source, final T[] destination, final int left, final int mid,
            final int right, final Comparator<? super T> comp) {
        int i = left, j = mid + 1;

        for (int k = left; k <= right; k++) {
            if (i == mid + 1) {
                destination[k] = source[j++];
                continue;
            } else if (j == right + 1) {
                destination[k] = source[i++];
                continue;
            } else {
                destination[k] = comp.compare(source[i], source[j]) <= 0 ? source[i++] : source[j++];
            }
        }
    }

    /**
     * Performs an in-place shell sort (extension of insertion sort) of the provided data.
     *
     * @param <T>
     *            the type of data to sort
     * @param array
     *            our source data
     * @param left
     *            the left index of the range to sort
     * @param right
     *            the right index (inclusive) of the range to sort
     * @param comp
     *            our object Comparator
     */
    public static <T> void shellSort(final T[] array, final int left, final int right,
            final Comparator<? super T> comp) {
        int h;
        for (h = 1; h <= (right - 1) / 9; h = 3 * h + 1) {
            ;
        }
        for (; h > 0; h /= 3) {
            for (int i = left + h; i <= right; i++) {
                int j = i;
                final T val = array[i];
                while (j >= left + h && comp.compare(val, array[j - h]) < 0) {
                    array[j] = array[j - h];
                    j -= h;
                }
                array[j] = val;
            }
        }
    }

    /**
     * Performs an in-place shell sort (extension of insertion sort) of the provided data.
     *
     * @param <T>
     *            the type of data to sort
     * @param array
     *            our source data
     * @param left
     *            the left index of the range to sort
     * @param right
     *            the right index (inclusive) of the range to sort
     */
    public static <T extends Comparable<T>> void shellSort(final T[] array, final int left, final int right) {
        int h;
        for (h = 1; h <= (right - 1) / 9; h = 3 * h + 1) {
            ;
        }
        for (; h > 0; h /= 3) {
            for (int i = left + h; i <= right; i++) {
                int j = i;
                final T val = array[i];
                while (j >= left + h && val.compareTo(array[j - 1]) < 0) {
                    array[j] = array[j - h];
                    j -= h;
                }
                array[j] = val;
            }
        }
    }
}
