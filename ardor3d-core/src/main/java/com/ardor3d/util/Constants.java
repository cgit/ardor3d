/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it 
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.util;

/**
 * Just a simple flag holder for runtime stripping of various ardor3d logging and debugging features.
 */
public class Constants {

    public static boolean updateGraphs = false;

    public static final boolean useStatePools = System.getProperty("ardor3d.noStatePools") == null;

    public static final boolean stats = System.getProperty("ardor3d.stats") != null;

    public static final boolean trackDirectMemory = System.getProperty("ardor3d.trackDirect") != null;

    public static final boolean useMultipleContexts = System.getProperty("ardor3d.useMultipleContexts") != null;

    public static final boolean storeSavableImages = System.getProperty("ardor3d.storeSavableImages") != null;

    public static final int maxStatePoolSize = Integer.parseInt(System.getProperty("ardor3d.maxStatePoolSize", "11"));

    public static final boolean useValidatingTransform = System.getProperty("ardor3d.disableValidatingTransform") == null;

    public static final boolean enableInstancedGeometrySupport = System.getProperty("ardor3d.enableInstancedGeometrySupport") != null;
}
