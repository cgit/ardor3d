/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */
package com.ardor3d.audio;

public interface IStreamListener
{
    /**
     * Notifies implementation that an End Of Stream was reached.
     * @param sourcename String identifier of the source which reached the EOS.
     * @param queueSize Number of items left the the stream's play queue, or zero if none.
    */
    public void endOfStream( String sourcename, int queueSize );
}
