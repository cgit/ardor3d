/**
 * Copyright (c) 2008-2023 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.extension.model.off;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.ardor3d.extension.model.off.OffDimensionInfo.OffKeywordVertexDataComponentId;
import com.ardor3d.math.ColorRGBA;
import com.ardor3d.math.Vector2;
import com.ardor3d.math.Vector3;
import com.ardor3d.util.geom.GeometryTool;
import com.ardor3d.util.resource.ResourceLocator;
import com.ardor3d.util.resource.ResourceLocatorTool;
import com.ardor3d.util.resource.ResourceSource;

/**
 * OFF importer. See <a href="http://paulbourke.net/dataformats/off/">the format spec</a> and
 * <a href="http://www.geomview.org/docs/html/OFF.html">Geomview's documentation</a>
 *
 * N.B: WORK IN PROGRESS, STILL A LOT OF WORK TO DO
 *
 * N.B: supports only the ASCII file format as there's a lack of available binary files to make some tests and the
 * specification mentions some constants in the include file named "off.h" but it's not included in Geomview's source
 * code. It supports only 1D, 2D and 3D without homogeneous coordinates. It supports only triangles and quads.
 */
public class OffImporter {

    private static final Logger LOGGER = Logger.getLogger(OffImporter.class.getName());

    public static class OffFileParser extends StreamTokenizer implements Closeable {

        private final Reader reader;

        /**
         * Constructor.
         *
         * @param reader
         *            The Reader.
         */
        public OffFileParser(final Reader reader) {
            super(reader);
            this.reader = reader;
            resetSyntax();
            eolIsSignificant(true);
            // doesn't modify the case to avoid ambiguities between nOFF and NOFF
            lowerCaseMode(false);

            // all printable ascii characters
            wordChars('!', '~');

            whitespaceChars(' ', ' ');
            whitespaceChars('\n', '\n');
            whitespaceChars('\r', '\r');
            whitespaceChars('\t', '\t');

            commentChar('#');
        }

        @Override
        public void close() throws IOException {
            reader.close();
        }
    }

    private ResourceLocator _modelLocator;

    /**
     * optional indexed color map
     */
    private final Map<Integer, ColorRGBA> _colorMap;

    /**
     * Constructor.
     */
    public OffImporter() {
        this(null);
    }

    public OffImporter(final Map<Integer, ColorRGBA> colorMap) {
        super();
        _colorMap = colorMap;
    }

    /**
     * Reads a OFF file from the given resource
     *
     * @param resource
     *            the name of the resource to find.
     *
     * @return a OffGeometryStore data object containing the scene and other useful elements.
     */
    public OffGeometryStore load(final String resource) {
        return load(resource, new GeometryTool());
    }

    /**
     * Reads a OFF file from the given resource
     *
     * @param resource
     *            the name of the resource to find.
     * @param geometryTool
     *            the geometry tool to optimize the meshes
     *
     * @return a OffGeometryStore data object containing the scene and other useful elements.
     */
    public OffGeometryStore load(final String resource, final GeometryTool geometryTool) {
        final ResourceSource source;
        if (_modelLocator == null) {
            source = ResourceLocatorTool.locateResource(ResourceLocatorTool.TYPE_MODEL, resource);
        } else {
            source = _modelLocator.locateResource(resource);
        }

        if (source == null) {
            throw new Error("Unable to locate '" + resource + "'");
        }

        return load(source, geometryTool);
    }

    /**
     * Reads a OFF file from the given resource
     *
     * @param resource
     *            the resource to find.
     *
     * @return a OffGeometryStore data object containing the scene and other useful elements.
     */
    public OffGeometryStore load(final ResourceSource resource) {
        return load(resource, new GeometryTool());
    }

    /**
     * Reads a OFF file from the given resource
     *
     * @param resource
     *            the resource to find.
     * @param geometryTool
     *            the geometry tool to optimize the meshes
     *
     * @return a OffGeometryStore data object containing the scene and other useful elements.
     */
    @SuppressWarnings("resource")
    public OffGeometryStore load(final ResourceSource resource, final GeometryTool geometryTool) {
        final OffGeometryStore store = createGeometryStore(geometryTool);
        try {
            try (final OffFileParser parser = new OffFileParser(
                    new BufferedReader(new InputStreamReader(resource.openStream(), StandardCharsets.US_ASCII)))) {
                try {
                    final Integer numberOfVertices;
                    final Integer numberOfFaces;
                    final Integer numberOfEdges;
                    // starts reading the file as ascii (binary file not supported)
                    // skips the commented line(s)
                    do {
                        parser.nextToken();
                    } while (parser.ttype != StreamTokenizer.TT_WORD && parser.ttype != StreamTokenizer.TT_EOF);
                    // if the end of file is reached too early
                    if (parser.ttype == StreamTokenizer.TT_EOF) {
                        throw new IOException(
                                "Premature end of file, expected an optional off keyword followed by three integers vertex_count face_count edge_count");
                    }
                    final String unhandledFirstParsedValue;
                    // tries to read an (optional) off keyword
                    final OffDimensionInfo offDimensionInfo = new OffDimensionInfo(parser.sval);
                    if (offDimensionInfo.hasHomogeneousComponent()) {
                        throw new IOException(
                                "Homogeneous coordinates aren't supported whereas " + parser.sval + " requires it");
                    }
                    if (offDimensionInfo.isDeduced()) {
                        // no *off keyword
                        OffImporter.LOGGER.log(Level.FINE, "No off keyword on line " + parser.lineno());
                        unhandledFirstParsedValue = parser.sval;
                    } else {
                        OffImporter.LOGGER.log(Level.FINE, parser.sval + " keyword on line " + parser.lineno());
                        unhandledFirstParsedValue = null;
                    }
                    parser.nextToken();
                    if (parser.ttype == StreamTokenizer.TT_EOF) {
                        throw new IOException(
                                "Premature end of file, expected an optional off keyword followed by three integers vertex_count face_count edge_count");
                    }
                    if (parser.ttype == StreamTokenizer.TT_EOL) {
                        if (unhandledFirstParsedValue == null) {
                            // reads the 3 integer values on the second uncommented line, most common scenario:
                            // *OFF
                            // # comment
                            // # another comment
                            // # etc
                            // vertex_count face_count edge_count
                            // skips the commented line(s)
                            do {
                                parser.nextToken();
                            } while (parser.ttype != StreamTokenizer.TT_WORD && parser.ttype != StreamTokenizer.TT_EOF);
                            // if the end of file is reached too early
                            if (parser.ttype == StreamTokenizer.TT_EOF) {
                                throw new IOException(
                                        "Premature end of line, expected three integers vertex_count face_count edge_count");
                            } else {
                                if (offDimensionInfo.needDimensionNumber()) {
                                    offDimensionInfo.useDimensionNumber(Integer.parseInt(parser.sval));
                                    do {
                                        parser.nextToken();
                                    } while (parser.ttype != StreamTokenizer.TT_WORD
                                            && parser.ttype != StreamTokenizer.TT_EOF);
                                    if (parser.ttype == StreamTokenizer.TT_EOF) {
                                        throw new IOException(
                                                "Premature end of line, expected three integers vertex_count face_count edge_count");
                                    }
                                }
                                numberOfVertices = Integer.valueOf(parser.sval);
                                parser.nextToken();
                            }
                        } else {
                            throw new IOException(
                                    "Premature end of line, expected three integers vertex_count face_count edge_count");
                        }
                    } else {
                        if (unhandledFirstParsedValue == null) {
                            if (offDimensionInfo.needDimensionNumber()) {
                                offDimensionInfo.useDimensionNumber(Integer.parseInt(parser.sval));
                                do {
                                    parser.nextToken();
                                } while (parser.ttype != StreamTokenizer.TT_WORD
                                        && parser.ttype != StreamTokenizer.TT_EOF);
                                if (parser.ttype == StreamTokenizer.TT_EOF) {
                                    throw new IOException(
                                            "Premature end of line, expected three integers vertex_count face_count edge_count");
                                }
                            }
                            // *off keyword and three integers on the first line, rare scenario:
                            // *OFF vertex_count face_count edge_count
                            // reads the 3 integer values on the first uncommented line, most common scenario
                            numberOfVertices = Integer.valueOf(parser.sval);
                            parser.nextToken();
                        } else {
                            // no *off keyword, three integers on the first line, very improbable scenario:
                            // vertex_count face_count edge_count
                            // read the 2 next integer values on the first line
                            numberOfVertices = Integer.valueOf(unhandledFirstParsedValue);
                        }
                    }
                    if (parser.ttype == StreamTokenizer.TT_WORD) {
                        numberOfFaces = Integer.valueOf(parser.sval);
                    } else {
                        throw new IOException(
                                "Premature end of line, expected three integers vertex_count face_count edge_count");
                    }
                    parser.nextToken();
                    if (parser.ttype == StreamTokenizer.TT_WORD) {
                        numberOfEdges = Integer.valueOf(parser.sval);
                        OffImporter.LOGGER.log(Level.FINE, "Number of edges: " + numberOfEdges);
                    } else {
                        throw new IOException(
                                "Premature end of line, expected three integers vertex_count face_count edge_count");
                    }
                    // reads until the end of line is reached (expected)
                    do {
                        parser.nextToken();
                    } while (parser.ttype != StreamTokenizer.TT_WORD && parser.ttype != StreamTokenizer.TT_EOF
                            && parser.ttype != StreamTokenizer.TT_EOL);
                    if (parser.ttype == StreamTokenizer.TT_WORD) {
                        throw new IOException("Unexpected word " + parser.sval);
                    }
                    if (parser.ttype == StreamTokenizer.TT_EOF) {
                        throw new IOException("Premature end of file, no vertex has been declared");
                    }
                    // before going further, ensures that the number of components inside the vertices is supported
                    if (offDimensionInfo.getVertexValuesPerTuple() < 1
                            || 3 < offDimensionInfo.getVertexValuesPerTuple()) {
                        throw new IOException("The off keyword " + parser.sval
                                + " requires the unsupported number of components per vertex or dimension "
                                + offDimensionInfo.getVertexValuesPerTuple());
                    }
                    // reads the vertices, normals, colors and/or texture coordinates for each vertex in that order
                    // FIXME handle other orders
                    for (int vertexIndex = 0; vertexIndex < numberOfVertices.intValue(); vertexIndex++) {
                        // skips comment lines, comments and empty lines
                        do {
                            parser.nextToken();
                        } while (parser.ttype != StreamTokenizer.TT_WORD && parser.ttype != StreamTokenizer.TT_EOF);
                        if (parser.ttype == StreamTokenizer.TT_EOF) {
                            throw new IOException("Premature end of file, expected " + numberOfVertices.intValue()
                                    + " vertices, found " + vertexIndex);
                        }
                        // keeps the current word
                        parser.pushBack();
                        // reads as much numbers as possible on the line
                        boolean goOn = true;
                        final List<Number> valueList = new ArrayList<>();
                        do {
                            parser.nextToken();
                            switch (parser.ttype) {
                                case StreamTokenizer.TT_WORD:
                                    if (parser.sval.contains(",") || parser.sval.contains(".")) {
                                        valueList.add(Double.valueOf(parser.sval));
                                    } else {
                                        valueList.add(Integer.valueOf(parser.sval));
                                    }
                                    break;
                                case StreamTokenizer.TT_EOL:
                                    goOn = false;
                                    break;
                                default:
                                    // the premature end of file is handled elsewhere, keeps the current state
                                    parser.pushBack();
                                    break;
                            }
                        } while (goOn);
                        final int colorValuesPerTuple = offDimensionInfo.computeColorValuesPerTuple(valueList.size());
                        OffImporter.LOGGER.log(Level.FINE,
                                "Coords: " + valueList.stream().map(Number::toString).collect(Collectors.joining(" ")));
                        switch (offDimensionInfo.getVertexValuesPerTuple()) {
                            case 1:
                                store.getDataStore().getVertices()
                                        .add(new Vector3(valueList.get(0).doubleValue(), 0.0d, 0.0d));
                            case 2:
                                store.getDataStore().getVertices().add(new Vector3(valueList.get(0).doubleValue(),
                                        valueList.get(1).doubleValue(), 0.0d));
                                break;
                            case 3:
                                store.getDataStore().getVertices().add(new Vector3(valueList.get(0).doubleValue(),
                                        valueList.get(1).doubleValue(), valueList.get(2).doubleValue()));
                                break;
                        }
                        int nextIndex = offDimensionInfo.getVertexValuesPerTuple();
                        for (final OffKeywordVertexDataComponentId vertexDataComponentId : offDimensionInfo
                                .getComponentSet()) {
                            switch (vertexDataComponentId) {
                                case NORMAL:
                                    switch (offDimensionInfo.getNormalValuesPerTuple()) {
                                        case 0:
                                            // nothing to do
                                            break;
                                        case 1:
                                            store.getDataStore().getNormals().add(
                                                    new Vector3(valueList.get(nextIndex).doubleValue(), 0.0d, 0.0d));
                                            break;
                                        case 2:
                                            store.getDataStore().getNormals()
                                                    .add(new Vector3(valueList.get(nextIndex).doubleValue(),
                                                            valueList.get(nextIndex + 1).doubleValue(), 0.0d));
                                            break;
                                        case 3:
                                            store.getDataStore().getNormals()
                                                    .add(new Vector3(valueList.get(nextIndex).doubleValue(),
                                                            valueList.get(nextIndex + 1).doubleValue(),
                                                            valueList.get(nextIndex + 2).doubleValue()));
                                            break;
                                    }
                                    nextIndex += offDimensionInfo.getNormalValuesPerTuple();
                                    break;
                                case COLOR:
                                    switch (colorValuesPerTuple) {
                                        case 0:
                                            // nothing to do
                                            break;
                                        case 1:
                                            final int colorIndex = valueList.get(nextIndex).intValue();
                                            if (_colorMap == null) {
                                                OffImporter.LOGGER.log(Level.WARNING,
                                                        "Color index found but no color map passed to the importer");
                                            } else {
                                                final ColorRGBA indexedRgba = _colorMap
                                                        .get(Integer.valueOf(colorIndex));
                                                if (indexedRgba == null) {
                                                    OffImporter.LOGGER.log(Level.WARNING, "No color found at the index "
                                                            + colorIndex + " in the color map");
                                                } else {
                                                    store.getDataStore().getColors().add(indexedRgba);
                                                }
                                            }
                                            break;
                                        case 3:
                                            final ColorRGBA rgb = new ColorRGBA(valueList.get(nextIndex).floatValue(),
                                                    valueList.get(nextIndex + 1).floatValue(),
                                                    valueList.get(nextIndex + 2).floatValue(), 0.0f);
                                            if (valueList.get(nextIndex) instanceof Integer) {
                                                rgb.divideLocal(255.0f);
                                            }
                                            rgb.setAlpha(1.0f);
                                            store.getDataStore().getColors().add(rgb);
                                            break;
                                        case 4:
                                            final ColorRGBA rgba = new ColorRGBA(valueList.get(nextIndex).floatValue(),
                                                    valueList.get(nextIndex + 1).floatValue(),
                                                    valueList.get(nextIndex + 2).floatValue(),
                                                    valueList.get(nextIndex + 3).floatValue());
                                            if (valueList.get(nextIndex) instanceof Integer) {
                                                rgba.divideLocal(255.0f);
                                            }
                                            store.getDataStore().getColors().add(rgba);
                                            break;
                                    }
                                    nextIndex += colorValuesPerTuple;
                                    break;
                                case TEXTURE:
                                    if (offDimensionInfo.getTextureValuesPerTuple() == 2) {
                                        store.getDataStore().getTextureCoordinates()
                                                .add(new Vector2(valueList.get(nextIndex).doubleValue(),
                                                        valueList.get(nextIndex + 1).doubleValue()));
                                    }
                                    nextIndex += offDimensionInfo.getTextureValuesPerTuple();
                                    break;
                            }
                        }
                    }
                    for (int faceIndex = 0; faceIndex < numberOfFaces.intValue(); faceIndex++) {
                        // skips comment lines, comments and empty lines
                        do {
                            parser.nextToken();
                        } while (parser.ttype != StreamTokenizer.TT_WORD && parser.ttype != StreamTokenizer.TT_EOF);
                        if (parser.ttype == StreamTokenizer.TT_EOF) {
                            throw new IOException("Premature end of file, expected " + numberOfFaces.intValue()
                                    + " faces, found " + faceIndex);
                        }
                        // keeps the current word
                        parser.pushBack();
                        // reads as much numbers as possible on the line
                        boolean goOn = true;
                        final List<Number> valueList = new ArrayList<>();
                        do {
                            parser.nextToken();
                            switch (parser.ttype) {
                                case StreamTokenizer.TT_WORD:
                                    if (parser.sval.contains(",") || parser.sval.contains(".")) {
                                        valueList.add(Double.valueOf(parser.sval));
                                    } else {
                                        valueList.add(Integer.valueOf(parser.sval));
                                    }
                                    break;
                                case StreamTokenizer.TT_EOL:
                                    goOn = false;
                                    break;
                                default:
                                    // the premature end of file is handled elsewhere, keeps the current state
                                    parser.pushBack();
                                    break;
                            }
                        } while (goOn);
                        if (valueList.isEmpty()) {
                            throw new IOException(
                                    "Premature end of (face) line, expected at least one value, got zero");
                        } else {
                            OffImporter.LOGGER.log(Level.FINE, "Face coords: "
                                    + valueList.stream().map(Number::toString).collect(Collectors.joining(" ")));
                            // puts the indices into the geometry store
                            final int vertexIndexCount = valueList.get(0).intValue();
                            final OffFaceInfo faceInfo = new OffFaceInfo();
                            IntStream.rangeClosed(1, vertexIndexCount).mapToObj(valueList::get)
                                    .mapToInt(Number::intValue).forEachOrdered(faceInfo::addVertexIndex);
                            final int colorComponentCount = valueList.size() - vertexIndexCount - 1;
                            switch (colorComponentCount) {
                                case 0:
                                    // no declared color
                                    break;
                                case 1:
                                    final int colorIndex = valueList.get(vertexIndexCount + 1).intValue();
                                    if (_colorMap == null) {
                                        OffImporter.LOGGER.log(Level.WARNING,
                                                "Color index found but no color map passed to the importer");
                                    } else {
                                        final ColorRGBA indexedRgba = _colorMap.get(Integer.valueOf(colorIndex));
                                        if (indexedRgba == null) {
                                            OffImporter.LOGGER.log(Level.WARNING,
                                                    "No color found at the index " + colorIndex + " in the color map");
                                        } else {
                                            faceInfo.setColor(indexedRgba);
                                        }
                                    }
                                    break;
                                case 3:
                                    final ColorRGBA rgb = new ColorRGBA(
                                            valueList.get(vertexIndexCount + 1).floatValue(),
                                            valueList.get(vertexIndexCount + 2).floatValue(),
                                            valueList.get(vertexIndexCount + 3).floatValue(), 0.0f);
                                    if (valueList.get(vertexIndexCount + 1) instanceof Integer) {
                                        rgb.divideLocal(255.0f);
                                    }
                                    rgb.setAlpha(1.0f);
                                    faceInfo.setColor(rgb);
                                    break;
                                case 4:
                                    final ColorRGBA rgba = new ColorRGBA(
                                            valueList.get(vertexIndexCount + 1).floatValue(),
                                            valueList.get(vertexIndexCount + 2).floatValue(),
                                            valueList.get(vertexIndexCount + 3).floatValue(),
                                            valueList.get(vertexIndexCount + 4).floatValue());
                                    if (valueList.get(vertexIndexCount + 1) instanceof Integer) {
                                        rgba.divideLocal(255.0f);
                                    }
                                    faceInfo.setColor(rgba);
                                    break;
                            }
                            store.addFace(faceInfo);
                        }
                    }
                    /**
                     * The faces must have a consistent color behavior: either they all have a color or none has a
                     * color. If some faces are colored and some aren't, it gives them a default color
                     */
                    store.replaceNullFaceColorsByDefaultFaceColorIfColoredFaces();
                } catch (final IOException ioe) {
                    throw new Exception("IO Error on line " + parser.lineno(), ioe);
                }
            }
        } catch (final Throwable t) {
            throw new Error("Unable to load off resource from URL: " + resource, t);
        }
        store.commitObjects();
        store.cleanup();
        return store;
    }

    protected OffGeometryStore createGeometryStore(final GeometryTool geometryTool) {
        return new OffGeometryStore(geometryTool);
    }

    public void setModelLocator(final ResourceLocator locator) {
        _modelLocator = locator;
    }
}
