/**
 * Copyright (c) 2008-2023 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.extension.model.off;

import java.util.ArrayList;
import java.util.List;

import com.ardor3d.math.ColorRGBA;

public class OffFaceInfo {

    private List<Integer> _vertexIndices;

    private ColorRGBA _color;

    public OffFaceInfo() {
        super();
    }

    public void addVertexIndex(final int vertexIndex) {
        if (_vertexIndices == null) {
            _vertexIndices = new ArrayList<>();
        }
        _vertexIndices.add(Integer.valueOf(vertexIndex));
    }

    public List<Integer> getVertexIndices() {
        return _vertexIndices;
    }

    public void setColor(final ColorRGBA color) {
        _color = color;
    }

    public ColorRGBA getColor() {
        return _color;
    }
}
