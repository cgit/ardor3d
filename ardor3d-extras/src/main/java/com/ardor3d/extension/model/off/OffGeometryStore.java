/**
 * Copyright (c) 2008-2023 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.extension.model.off;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ardor3d.image.Texture;
import com.ardor3d.math.ColorRGBA;
import com.ardor3d.math.Vector2;
import com.ardor3d.math.Vector3;
import com.ardor3d.renderer.IndexMode;
import com.ardor3d.renderer.state.TextureState;
import com.ardor3d.scenegraph.IndexBufferData;
import com.ardor3d.scenegraph.Mesh;
import com.ardor3d.scenegraph.Node;
import com.ardor3d.util.geom.BufferUtils;
import com.ardor3d.util.geom.GeometryTool;
import com.ardor3d.util.geom.GeometryTool.MatchCondition;

public class OffGeometryStore {

    private static final Logger LOGGER = Logger.getLogger(OffGeometryStore.class.getName());

    private int _totalMeshes = 0;

    private final OffDataStore _dataStore;

    private final Node _root;

    private final List<OffMaterial> _materialLibrary;

    private List<OffFaceInfo> _offFaceInfoList;

    private Texture _texture;

    private String _textureName;

    private final GeometryTool _geometryTool;

    public OffGeometryStore() {
        this(new GeometryTool());
    }

    public OffGeometryStore(final GeometryTool geometryTool) {
        super();
        _dataStore = new OffDataStore();
        _root = new Node();
        _materialLibrary = new ArrayList<>();
        _geometryTool = geometryTool;
    }

    public OffDataStore getDataStore() {
        return _dataStore;
    }

    public Node getScene() {
        return _root;
    }

    public List<OffMaterial> getMaterialLibrary() {
        return _materialLibrary;
    }

    public String getTextureName() {
        return _textureName;
    }

    public void setTextureName(final String textureName) {
        _textureName = textureName;
    }

    public Texture getTexture() {
        return _texture;
    }

    public void setTexture(final Texture texture) {
        _texture = texture;
    }

    public TextureState getTextureState() {
        if (_texture != null) {
            final TextureState tState = new TextureState();
            tState.setTexture(_texture, 0);
            return tState;
        }
        return null;
    }

    void addFace(final OffFaceInfo faceInfo) {
        if (_offFaceInfoList == null) {
            _offFaceInfoList = new ArrayList<>();
        }
        _offFaceInfoList.add(faceInfo);
    }

    void replaceNullFaceColorsByDefaultFaceColorIfColoredFaces() {
        // if there's at least one colored face
        if (_offFaceInfoList != null && !_offFaceInfoList.isEmpty()
                && _offFaceInfoList.stream().anyMatch((final OffFaceInfo faceInfo) -> faceInfo.getColor() != null)) {
            _offFaceInfoList.stream().forEach((final OffFaceInfo faceInfo) -> {
                // if a face has no color
                if (faceInfo.getColor() == null) {
                    // sets a default color
                    faceInfo.setColor(new ColorRGBA(0.666f, 0.666f, 0.666f, 0.666f));
                }
            });
        }
    }

    @SuppressWarnings("null")
    void commitObjects() {
        if (_offFaceInfoList != null) {
            final String name = "off_mesh" + _totalMeshes;
            final Mesh mesh = new Mesh(name);
            boolean hasColorsInFaces = false;
            final boolean hasTexCoords = _dataStore.getTextureCoordinates() != null
                    && !_dataStore.getTextureCoordinates().isEmpty();
            final boolean hasNormals = _dataStore.getNormals() != null && !_dataStore.getNormals().isEmpty();
            final boolean hasColorsInVertices = _dataStore.getColors() != null && !_dataStore.getColors().isEmpty();
            int vertexCount = 0;
            for (final OffFaceInfo offFaceInfo : _offFaceInfoList) {
                vertexCount += offFaceInfo.getVertexIndices().size();
                if (offFaceInfo.getColor() != null) {
                    hasColorsInFaces = true;
                }
            }
            final FloatBuffer vertices = BufferUtils.createVector3Buffer(vertexCount);
            final IndexBufferData<? extends Buffer> indices = BufferUtils.createIndexBufferData(vertexCount,
                    vertexCount - 1);

            final FloatBuffer normals = hasNormals ? BufferUtils.createFloatBuffer(vertices.capacity()) : null;
            final FloatBuffer colors = hasColorsInFaces || hasColorsInVertices
                    ? BufferUtils.createFloatBuffer(vertexCount * 4)
                    : null;
            final FloatBuffer uvs = hasTexCoords ? BufferUtils.createFloatBuffer(vertexCount * 2) : null;

            int dummyVertexIndex = 0;
            final List<IndexMode> indexModeList = new ArrayList<>();
            final List<Integer> indexLengthList = new ArrayList<>();
            for (final OffFaceInfo offFaceInfo : _offFaceInfoList) {
                final IndexMode previousIndexMode = indexModeList.isEmpty() ? null
                        : indexModeList.get(indexModeList.size() - 1);
                final IndexMode currentIndexMode;
                switch (offFaceInfo.getVertexIndices().size()) {
                    case 3: {
                        currentIndexMode = IndexMode.Triangles;
                        break;
                    }
                    case 4: {
                        currentIndexMode = IndexMode.Quads;
                        break;
                    }
                    default: {
                        currentIndexMode = null;
                        break;
                    }
                }
                if (currentIndexMode == null) {
                    OffGeometryStore.LOGGER.log(Level.SEVERE,
                            "The index mode cannot be determined for a face containing "
                                    + offFaceInfo.getVertexIndices().size() + " vertices");
                } else {
                    if (previousIndexMode == null || currentIndexMode != previousIndexMode) {
                        indexModeList.add(currentIndexMode);
                        indexLengthList.add(currentIndexMode.getVertexCount());
                    } else {
                        final int previousIndexLength = indexLengthList.get(indexLengthList.size() - 1).intValue();
                        final int currentIndexLength = previousIndexLength + currentIndexMode.getVertexCount();
                        indexLengthList.set(indexLengthList.size() - 1, Integer.valueOf(currentIndexLength));
                    }
                    for (final Integer vertexIndex : offFaceInfo.getVertexIndices()) {
                        indices.put(dummyVertexIndex);
                        final Vector3 vertex = _dataStore.getVertices().get(vertexIndex.intValue());
                        vertices.put(vertex.getXf()).put(vertex.getYf()).put(vertex.getZf());
                        if (hasNormals) {
                            final Vector3 normal = _dataStore.getNormals().get(vertexIndex.intValue());
                            normals.put(normal.getXf()).put(normal.getYf()).put(normal.getZf());
                        }
                        if (hasColorsInVertices) {
                            final ColorRGBA color = _dataStore.getColors().get(vertexIndex.intValue());
                            colors.put(color.getRed()).put(color.getGreen()).put(color.getBlue()).put(color.getAlpha());
                        }
                        if (hasTexCoords) {
                            final Vector2 texCoords = _dataStore.getTextureCoordinates().get(vertexIndex.intValue());
                            uvs.put(texCoords.getXf()).put(texCoords.getYf());
                        }
                        dummyVertexIndex++;
                    }
                    if (hasColorsInFaces) {
                        final ColorRGBA color = offFaceInfo.getColor();
                        colors.put(color.getRed()).put(color.getGreen()).put(color.getBlue()).put(color.getAlpha());
                    }
                }
            }

            vertices.rewind();
            mesh.getMeshData().setVertexBuffer(vertices);
            indices.rewind();
            mesh.getMeshData().setIndices(indices);
            if (indexModeList.size() == 1) {
                mesh.getMeshData().setIndexMode(indexModeList.get(0));
                mesh.getMeshData().setIndexLengths(null);
            } else {
                mesh.getMeshData().setIndexModes(indexModeList.toArray(new IndexMode[indexModeList.size()]));
                final int[] indexLengths = new int[indexLengthList.size()];
                for (int indexLengthIndex = 0; indexLengthIndex < indexLengths.length; indexLengthIndex++) {
                    indexLengths[indexLengthIndex] = indexLengthList.get(indexLengthIndex).intValue();
                }
                mesh.getMeshData().setIndexLengths(indexLengths);
            }
            final EnumSet<MatchCondition> matchConditions = EnumSet.noneOf(MatchCondition.class);
            if (hasNormals) {
                normals.rewind();
                mesh.getMeshData().setNormalBuffer(normals);
                matchConditions.add(MatchCondition.Normal);
            }
            if (hasColorsInVertices || hasColorsInFaces) {
                colors.rewind();
                mesh.getMeshData().setColorBuffer(colors);
                matchConditions.add(MatchCondition.Color);
            }
            if (hasTexCoords) {
                uvs.rewind();
                mesh.getMeshData().setTextureBuffer(uvs, 0);
                matchConditions.add(MatchCondition.UVs);
            }

            if (indexModeList.size() == 1) {
                _geometryTool.minimizeVerts(mesh, matchConditions);
            } else {
                // FIXME unsure about minimizeVerts preserving the index modes
            }

            final TextureState tState = getTextureState();
            if (tState != null) {
                mesh.setRenderState(tState);
            }

            mesh.updateModelBound();
            _root.attachChild(mesh);
            _totalMeshes++;
            _offFaceInfoList = null;
        }
    }

    void cleanup() {
        _offFaceInfoList = null;
    }
}
