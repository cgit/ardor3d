/**
 * Copyright (c) 2008-2023 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.extension.model.off;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * off dimension information, i.e sizes of the components
 */
final class OffDimensionInfo {

    enum OffKeywordVertexDataComponentId {
        /**
         * color
         */
        COLOR,
        /**
         * normal
         */
        NORMAL,
        /**
         * texture
         */
        TEXTURE;
    }

    /**
     * The off keyword indicates which data is available for each vertex except vertex coordinates which are mandatory
     */
    private enum OffKeyword {
        /**
         * 3D
         */
        _OFF(3, false,
                Stream.<OffKeywordVertexDataComponentId> empty().collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D
         */
        _4OFF(4, true,
                Stream.<OffKeywordVertexDataComponentId> empty().collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D
         */
        _nOFF(-1, false,
                Stream.<OffKeywordVertexDataComponentId> empty().collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D
         */
        _4nOFF(-1, true,
                Stream.<OffKeywordVertexDataComponentId> empty().collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, colors
         */
        _COFF(3, false,
                Stream.of(OffKeywordVertexDataComponentId.COLOR).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, colors
         */
        _C4OFF(4, true,
                Stream.of(OffKeywordVertexDataComponentId.COLOR).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors
         */
        _CnOFF(-1, false,
                Stream.of(OffKeywordVertexDataComponentId.COLOR).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors
         */
        _C4nOFF(-1, true,
                Stream.of(OffKeywordVertexDataComponentId.COLOR).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, colors + normals
         */
        _CNOFF(3, false, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, colors + normals
         */
        _CN4OFF(4, true, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + normals
         */
        _CNnOFF(-1, false, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + normals
         */
        _CN4nOFF(-1, true, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, colors + normals + textures
         */
        _CNSTOFF(
                3, false, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, colors + normals + textures
         */
        _CNST4OFF(
                4, true, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + normals + textures
         */
        _CNSTnOFF(
                -1, false, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + normals + textures
         */
        _CNST4nOFF(
                -1, true, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, colors + textures
         */
        _CSTOFF(3, false, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, colors + textures
         */
        _CST4OFF(4, true, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + textures
         */
        _CSTnOFF(-1, false, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + textures
         */
        _CST4nOFF(-1, true, Stream.of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, colors + textures + normals
         */
        _CSTNOFF(
                3, false, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, colors + textures + normals
         */
        _CSTN4OFF(
                4, true, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + textures + normals
         */
        _CSTNnOFF(
                -1, false, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, colors + textures + normals
         */
        _CSTN4nOFF(
                -1, true, Stream
                        .of(OffKeywordVertexDataComponentId.COLOR, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, normals
         */
        _NOFF(3, false,
                Stream.of(OffKeywordVertexDataComponentId.NORMAL).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, normals
         */
        _N4OFF(4, true,
                Stream.of(OffKeywordVertexDataComponentId.NORMAL).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals
         */
        _NnOFF(-1, false,
                Stream.of(OffKeywordVertexDataComponentId.NORMAL).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals
         */
        _N4nOFF(-1, true,
                Stream.of(OffKeywordVertexDataComponentId.NORMAL).collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, normals + colors
         */
        _NCOFF(3, false, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, normals + colors
         */
        _NC4OFF(4, true, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + colors
         */
        _NCnOFF(-1, false, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + colors
         */
        _NC4nOFF(-1, true, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, normals + colors + textures
         */
        _NCSTOFF(
                3, false, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, normals + colors + textures
         */
        _NCST4OFF(
                4, true, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + colors + textures
         */
        _NCSTnOFF(
                -1, false, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + colors + textures
         */
        _NCST4nOFF(
                -1, true, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.TEXTURE)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, normals + textures
         */
        _NSTOFF(3, false, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, normals + textures
         */
        _NST4OFF(4, true, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + textures
         */
        _NSTnOFF(-1, false, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + textures
         */
        _NST4nOFF(-1, true, Stream.of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, normals + textures + colors
         */
        _NSTCOFF(
                3, false, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, normals + textures + colors
         */
        _NSTC4OFF(
                4, true, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + textures + colors
         */
        _NSTCnOFF(
                -1, false, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, normals + textures + colors
         */
        _NSTC4nOFF(
                -1, true, Stream
                        .of(OffKeywordVertexDataComponentId.NORMAL, OffKeywordVertexDataComponentId.TEXTURE,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, textures
         */
        _STOFF(3, false, Stream.of(OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, textures
         */
        _ST4OFF(4, true, Stream.of(OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures
         */
        _STnOFF(-1, false, Stream.of(OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures
         */
        _ST4nOFF(-1, true, Stream.of(OffKeywordVertexDataComponentId.TEXTURE)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, textures + colors
         */
        _STCOFF(3, false, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, textures + colors
         */
        _STC4OFF(4, true, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + colors
         */
        _STCnOFF(-1, false, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + colors
         */
        _STC4nOFF(-1, true, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, textures + colors + normals
         */
        _STCNOFF(
                3, false, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, textures + colors + normals
         */
        _STCN4OFF(
                4, true, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + colors + normals
         */
        _STCNnOFF(
                -1, false, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + colors + normals
         */
        _STCN4nOFF(
                -1, true, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.COLOR,
                                OffKeywordVertexDataComponentId.NORMAL)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, textures + normals
         */
        _STNOFF(3, false, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, textures + normals
         */
        _STN4OFF(4, true, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + normals
         */
        _STNnOFF(-1, false, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + normals
         */
        _STN4nOFF(-1, true, Stream.of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL)
                .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 3D, textures + normals + colors
         */
        _STNCOFF(
                3, false, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * 4D, textures + normals + colors
         */
        _STNC4OFF(
                4, true, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + normals + colors
         */
        _STNCnOFF(
                -1, false, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new))),
        /**
         * ?D, textures + normals + colors
         */
        _STNC4nOFF(
                -1, true, Stream
                        .of(OffKeywordVertexDataComponentId.TEXTURE, OffKeywordVertexDataComponentId.NORMAL,
                                OffKeywordVertexDataComponentId.COLOR)
                        .collect(Collectors.toCollection(LinkedHashSet::new)));

        /**
         * set of components indicating whether colors, normals and textures are expected on the vertices
         */
        private final LinkedHashSet<OffKeywordVertexDataComponentId> componentSet;
        /**
         * text of the keyword as found in the very beginning of the file
         */
        private final String keywordText;

        /**
         * vertex values per tuple, either 3 or 4, -1 if unknown yet
         */
        private final int vertexValuesPerTuple;

        private final boolean homogeneousComponent;

        /**
         * minimum color values per tuple, either 0 or 1
         */
        private final int minColorValuesPerTuple;

        /**
         * maximum color values per tuple, either 0, 1 or 4, color map, RGB and RGBA colors are supported
         */
        private final int maxColorValuesPerTuple;

        /**
         * texture values per tuple, either 0 or 2
         */
        private final int textureValuesPerTuple;

        private OffKeyword(final int vertexValuesPerTuple, final boolean homogeneousComponent,
                final LinkedHashSet<OffKeywordVertexDataComponentId> componentSet) {
            this.vertexValuesPerTuple = vertexValuesPerTuple;
            this.componentSet = componentSet;
            // 1 color component for an index in a color map
            minColorValuesPerTuple = componentSet.contains(OffKeywordVertexDataComponentId.COLOR) ? 1 : 0;
            // 4 color components for RGBA
            maxColorValuesPerTuple = componentSet.contains(OffKeywordVertexDataComponentId.COLOR) ? 4 : 0;
            // u and v texture coordinates
            textureValuesPerTuple = componentSet.contains(OffKeywordVertexDataComponentId.TEXTURE) ? 2 : 0;
            keywordText = name().substring(1);
            this.homogeneousComponent = homogeneousComponent;
        }

        String getKeywordText() {
            return keywordText;
        }

        int getVertexValuesPerTuple() {
            return vertexValuesPerTuple;
        }

        int getMinColorValuesPerTuple() {
            return minColorValuesPerTuple;
        }

        int getMaxColorValuesPerTuple() {
            return maxColorValuesPerTuple;
        }

        int getTextureValuesPerTuple() {
            return textureValuesPerTuple;
        }

        LinkedHashSet<OffKeywordVertexDataComponentId> getComponentSet() {
            return componentSet;
        }

        boolean hasHomogeneousComponent() {
            return homogeneousComponent;
        }
    }

    private final OffKeyword offKeyword;

    /**
     * indicates whether the off keyword has been deduced and is not in the file
     */
    private final boolean deduced;

    private int vertexValuesPerTuple;

    private int normalValuesPerTuple;

    private int minValuesPerVertexLine;

    private int maxValuesPerVertexLine;

    /**
     * Constructor
     *
     * @param parsedText
     *            text of the off keyword
     */
    OffDimensionInfo(final String parsedText) {
        super();
        final OffKeyword offKeywordInFile = Arrays.stream(OffKeyword.values())
                .filter((final OffKeyword offKeyword) -> offKeyword.getKeywordText().equals(parsedText)).findFirst()
                .orElse(null);
        if (offKeywordInFile == null) {
            deduced = true;
            // when there is no off keyword in the file, it takes OFF keyword's behaviour
            offKeyword = OffKeyword._OFF;
        } else {
            deduced = false;
            offKeyword = offKeywordInFile;
        }
        if (offKeyword.getVertexValuesPerTuple() <= 0) {
            // it will have to be set later when nDim is known
            vertexValuesPerTuple = -1;
        } else {
            // number of dimensions from the off keyword
            vertexValuesPerTuple = offKeyword.getVertexValuesPerTuple();
            compute();
        }
    }

    private final void compute() {
        normalValuesPerTuple = offKeyword.getComponentSet().contains(OffKeywordVertexDataComponentId.NORMAL)
                ? vertexValuesPerTuple
                : 0;
        final int valuesPerVertexLineExcludingColorValues = vertexValuesPerTuple + normalValuesPerTuple
                + offKeyword.getTextureValuesPerTuple();
        minValuesPerVertexLine = valuesPerVertexLineExcludingColorValues + offKeyword.getMinColorValuesPerTuple();
        maxValuesPerVertexLine = valuesPerVertexLineExcludingColorValues + offKeyword.getMaxColorValuesPerTuple();
    }

    boolean needDimensionNumber() {
        return vertexValuesPerTuple == -1;
    }

    void useDimensionNumber(final int nDim) {
        if (vertexValuesPerTuple == -1) {
            if (nDim <= 0) {
                throw new IllegalArgumentException("Illegal dimension number " + nDim);
            }
            // if 4nOFF (4 => hasHomogeneousComponent(), nOFF => nDim), each vertex has Ndim+1 components
            vertexValuesPerTuple = nDim + (hasHomogeneousComponent() ? 1 : 0);
            compute();
        } else {
            throw new IllegalArgumentException("Illegal dimension number " + nDim
                    + ", unneeded when using the off keyword " + offKeyword.getKeywordText());
        }
    }

    boolean isDeduced() {
        return deduced;
    }

    int computeColorValuesPerTuple(final int totalValuesPerVertexLine) throws IOException {
        if (totalValuesPerVertexLine < minValuesPerVertexLine) {
            throw new IOException("Premature end of (vertex) line, expected at least " + minValuesPerVertexLine
                    + " values, got " + totalValuesPerVertexLine);
        }
        if (maxValuesPerVertexLine < totalValuesPerVertexLine) {
            throw new IOException("Too much values per (vertex) line, expected at most " + maxValuesPerVertexLine
                    + " values, got " + totalValuesPerVertexLine);
        }
        return totalValuesPerVertexLine - vertexValuesPerTuple - normalValuesPerTuple
                - offKeyword.textureValuesPerTuple;
    }

    int getVertexValuesPerTuple() {
        return vertexValuesPerTuple;
    }

    int getNormalValuesPerTuple() {
        return normalValuesPerTuple;
    }

    int getTextureValuesPerTuple() {
        return offKeyword.getTextureValuesPerTuple();
    }

    boolean hasHomogeneousComponent() {
        return offKeyword.hasHomogeneousComponent();
    }

    LinkedHashSet<OffKeywordVertexDataComponentId> getComponentSet() {
        return offKeyword.getComponentSet();
    }
}
