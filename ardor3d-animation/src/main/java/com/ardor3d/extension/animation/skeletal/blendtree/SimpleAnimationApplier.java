/**
 * Copyright (c) 2008-2014 Ardor Labs, Inc.
 *
 * This file is part of Ardor3D.
 *
 * Ardor3D is free software: you can redistribute it and/or modify it
 * under the terms of its license which may be found in the accompanying
 * LICENSE file or at <http://www.ardor3d.com/LICENSE>.
 */

package com.ardor3d.extension.animation.skeletal.blendtree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import com.ardor3d.extension.animation.skeletal.AnimationApplier;
import com.ardor3d.extension.animation.skeletal.AnimationManager;
import com.ardor3d.extension.animation.skeletal.SkeletonPose;
import com.ardor3d.extension.animation.skeletal.clip.JointData;
import com.ardor3d.extension.animation.skeletal.clip.TransformData;
import com.ardor3d.extension.animation.skeletal.clip.TriggerCallback;
import com.ardor3d.extension.animation.skeletal.clip.TriggerData;
import com.ardor3d.scenegraph.Node;
import com.ardor3d.scenegraph.Spatial;

/**
 * Very simple applier. Just applies joint transform data, calls any callbacks and updates the pose's global transforms.
 */
public class SimpleAnimationApplier implements AnimationApplier {

    private final Map<String, List<TriggerCallback>> _triggerCallbacks = new HashMap<>();

    private final Map<String, Spatial> _spatialCache = new WeakHashMap<>();

    @Override
    public void apply(final Spatial root, final AnimationManager manager) {
        if (root == null) {
            return;
        }
        final Map<String, ? extends Object> data = manager.getCurrentSourceData();

        // cycle through, pulling out and applying those we know about
        if (data != null) {
            for (final String key : data.keySet()) {
                final Object value = data.get(key);
                if (value instanceof JointData) { // ignore
                } else if (value instanceof TransformData) {
                    final TransformData transformData = (TransformData) value;
                    final Spatial applyTo = findChild(root, key);
                    if (applyTo != null) {
                        transformData.applyTo(applyTo);
                    }
                }
            }
        }
    }

    private Spatial findChild(final Spatial root, final String key) {
        if (_spatialCache.containsKey(key)) {
            return _spatialCache.get(key);
        }
        if (key.equals(root.getName())) {
            _spatialCache.put(key, root);
            return root;
        } else if (root instanceof Node) {
            final Spatial spat = ((Node) root).getChild(key);
            if (spat != null) {
                _spatialCache.put(key, spat);
                return spat;
            }
        }
        return null;
    }

    @Override
    public void applyTo(final SkeletonPose applyToPose, final AnimationManager manager) {
        final Map<String, ? extends Object> data = manager.getCurrentSourceData();

        // cycle through, pulling out and applying those we know about
        if (data != null) {
            for (final Object value : data.values()) {
                if (value instanceof JointData) {
                    final JointData jointData = (JointData) value;
                    if (jointData.getJointIndex() >= 0) {
                        jointData.applyTo(applyToPose.getLocalJointTransforms()[jointData.getJointIndex()]);
                    }
                } else if (value instanceof TriggerData) {
                    final TriggerData trigger = (TriggerData) value;
                    if (trigger.isArmed()) {
                        try {
                            // pull callback(s) for the current trigger key, if exists, and call.
                            for (final String curTrig : trigger.getCurrentTriggers()) {
                                final List<TriggerCallback> curTrigCallbackList = _triggerCallbacks.get(curTrig);
                                if (curTrigCallbackList != null) {
                                    for (final TriggerCallback cb : curTrigCallbackList) {
                                        cb.doTrigger(applyToPose, manager);
                                    }
                                }
                            }
                        } finally {
                            trigger.setArmed(false);
                        }
                    }
                }
            }

            applyToPose.updateTransforms();
        }
    }

    public void clearSpatialCache() {
        _spatialCache.clear();
    }

    @Override
    public void addTriggerCallback(final String key, final TriggerCallback callback) {
        _triggerCallbacks.compute(key, (final String realKey, final List<TriggerCallback> oldValue) -> {
            final List<TriggerCallback> newValue;
            if (oldValue == null) {
                newValue = new ArrayList<>();
            } else {
                newValue = oldValue;
            }
            newValue.add(callback);
            return newValue;
        });
    }

    @Override
    public boolean removeTriggerCallback(final String key, final TriggerCallback callback) {
        final boolean[] result = new boolean[] { false };
        _triggerCallbacks.compute(key, (final String realKey, final List<TriggerCallback> oldValue) -> {
            final List<TriggerCallback> newValue;
            if (oldValue == null) {
                newValue = null;
            } else {
                result[0] = oldValue.remove(callback);
                if (result[0] && oldValue.isEmpty()) {
                    newValue = null;
                } else {
                    newValue = oldValue;
                }
            }
            return newValue;
        });
        return result[0];
    }
}
