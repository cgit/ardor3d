
package com.ardor3d.extension.terrain.client;

import java.nio.ByteBuffer;
import java.util.Set;

import com.ardor3d.extension.terrain.util.Tile;

/**
 * Feeds texture data to a TextureCache
 */
public interface TextureSource {
    /**
     * Called to initialize and setup the texture clipmap.
     *
     * @return the configuration
     * @throws Exception
     *             exception
     */
    TextureConfiguration getConfiguration() throws Exception;

    /**
     * Returns which tiles that contain data in the requested region.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tileX
     *            the tile abscissa
     * @param tileY
     *            the tile ordinate
     * @param numTilesX
     *            the number of tiles on X
     * @param numTilesY
     *            the number of tiles on Y
     * @return the tiles that contain data in the requested region
     * @throws Exception
     *             exception
     */
    Set<Tile> getValidTiles(final int clipmapLevel, final int tileX, final int tileY, int numTilesX, int numTilesY)
            throws Exception;

    /**
     * Returns which tiles that should be marked as invalid and updated in the requested region.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tileX
     *            the tile abscissa
     * @param tileY
     *            the tile ordinate
     * @param numTilesX
     *            the number of tiles on X
     * @param numTilesY
     *            the number of tiles on Y
     * @return the tiles that should be marked as invalid and updated in the requested region
     * @throws Exception
     *             exception
     */
    Set<Tile> getInvalidTiles(final int clipmapLevel, final int tileX, final int tileY, int numTilesX, int numTilesY)
            throws Exception;

    /**
     * Returns the contributing source id for the requested tile.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tile
     *            the tile
     * @return the contributing source id for the requested tile
     */
    int getContributorId(int clipmapLevel, Tile tile);

    /**
     * Request for texture data for a tile.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tile
     *            the tile
     * @return the texture data for a tile
     * @throws Exception
     *             exception
     */
    ByteBuffer getTile(int clipmapLevel, final Tile tile) throws Exception;
}
