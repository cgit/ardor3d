
package com.ardor3d.extension.terrain.client;

import java.util.Set;

import com.ardor3d.extension.terrain.util.Tile;

/**
 * Feeds terrain data to a TerrainCache
 */
public interface TerrainSource {
    /**
     * Called to initialize and setup the geometry clipmap terrain.
     *
     * @return TerrainConfiguration
     * @throws Exception
     *             exception
     */
    TerrainConfiguration getConfiguration() throws Exception;

    /**
     * Returns which tiles that contain data in the requested region.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tileX
     *            the abscissa of the tile
     * @param tileY
     *            the ordinate of the tile
     * @param numTilesX
     *            the number of tiles on X
     * @param numTilesY
     *            the number of tiles on Y
     * @return the tiles that contain data in the requested region
     * @throws Exception
     *             exception
     */
    Set<Tile> getValidTiles(final int clipmapLevel, final int tileX, final int tileY, int numTilesX, int numTilesY)
            throws Exception;

    /**
     * Returns which tiles that should be marked as invalid and updated in the requested region.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tileX
     *            the abscissa of the tile
     * @param tileY
     *            the ordinate of the tile
     * @param numTilesX
     *            the number of tiles on X
     * @param numTilesY
     *            the number of tiles on Y
     * @return the tiles that should be marked as invalid and updated in the requested region
     * @throws Exception
     *             exception
     */
    Set<Tile> getInvalidTiles(final int clipmapLevel, final int tileX, final int tileY, int numTilesX, int numTilesY)
            throws Exception;

    /**
     * Returns the contributing source id for the requested tile.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tile
     *            the tile
     * @return the contributor identifier
     */
    int getContributorId(int clipmapLevel, Tile tile);

    /**
     * Request for height data for a tile.
     *
     * @param clipmapLevel
     *            the clipmap level
     * @param tile
     *            the tile
     * @return the height data for this tile
     * @throws Exception
     *             exception
     */
    float[] getTile(int clipmapLevel, final Tile tile) throws Exception;
}
